package pl.codementors.liczby;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Random;
import java.util.Scanner;

public class NumbersMain {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        double[] numbers = new double[0];
        Stats stats = null;
        boolean running = true;
        Scanner scanner = new Scanner(System.in);
        while (running) {
            String command = scanner.next();
            switch (command) {
                case "generate": {
                    generate(scanner);
                    break;
                }
                case "load": {
                    numbers = load(scanner);
                    break;
                }
                case "print": {
                    print(numbers);
                    break;
                }
                case "save": {
                    save(scanner, numbers);
                    break;
                }
                case "add": {
                    add(scanner, numbers);
                    break;
                }
                case "multiply": {
                    multiply(scanner, numbers);
                    break;
                }
                case "stat": {
                    stats = stat(numbers);
                    break;
                }
                case "print_stat": {
                    print(stats);
                    break;
                }
                case "save_stat": {
                    saveStats(scanner, stats);
                    break;
                }
                case "load_stat": {
                    stats = loadStats(scanner);
                    break;
                }
                case "quit": {
                    running = false;
                    break;
                }
            }
        }
    }

    private static Stats loadStats(Scanner scanner) {
        String file = scanner.next();
        try (FileInputStream fis = new FileInputStream(file);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            return (Stats) ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            System.err.println(ex);
            return null;
        }
    }

    private static void saveStats(Scanner scanner, Stats stats) {
        String file = scanner.next();
        try (FileOutputStream fos = new FileOutputStream(file);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(stats);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    private static void print(Stats stats) {
        System.out.println("Max: " + stats.getMax());
        System.out.println("Min: " + stats.getMin());
        System.out.println("Average: " + stats.getAverage());
    }

    private static Stats stat(double[] numbers) {
        double min = numbers[0];
        double max = numbers[0];
        double sum = 0;
        for (double n : numbers) {
            sum += n;
            if (n < min) {
                min = n;
            }
            if (n > max) {
                max = n;
            }
        }
        double average = sum / numbers.length;
        return new Stats(min, max, average);
    }

    private static void multiply(Scanner scanner, double[] numbers) {
        System.out.println("Value?");
        double value = scanner.nextDouble();
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] *= value;
        }
    }

    private static void add(Scanner scanner, double[] numbers) {
        System.out.println("Value?");
        double value = scanner.nextDouble();
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] += value;
        }
    }

    private static void print(double[] array) {
        for (double v : array) {
            System.out.println(v);
        }
    }

    private static double[] load(Scanner scanner) {
        System.out.println("From where?");
        String file = scanner.next();
        try (FileReader fr = new FileReader(file);
             BufferedReader br = new BufferedReader(fr)) {
            String countText = br.readLine();
            int count = Integer.parseInt(countText);
            double[] numbers = new double[count];
            for (int i = 0; i < count; i++) {
                String valueText = br.readLine();
                double value = Double.parseDouble(valueText);
                numbers[i] = value;
            }
            return numbers;
        } catch (IOException ex) {
            System.err.println(ex);
            return new double[0];
        }
    }

    private static void save(Scanner scanner, double[] array) {
        System.out.println("Where?");
        String file = scanner.next();
        try (FileWriter fw = new FileWriter(file)) {
            fw.write(array.length + "\n");
            for (int i = 0; i < array.length; i++) {
                fw.write(array[i] + "\n");
            }
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    private static void generate(Scanner scanner) {
        System.out.println("How many?");
        int count = scanner.nextInt();
        System.out.println("Where?");
        String file = scanner.next();
        Random random = new Random();
        try (FileWriter fw = new FileWriter(file)) {
            fw.write(count + "");
            fw.write("\n");
            for (int i = 0; i < count; i++) {
                double value = random.nextDouble() * 100;
                fw.write(value + "");
                fw.write("\n");
            }
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

}
